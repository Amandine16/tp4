function showHideDescription(){
  $(this).find(".description").toggle();
}

var weather = false;

// Obtenir la meteo
$.get( "https://api.openweathermap.org/data/2.5/weather?q=paris&appid=768a35a09a1701be84498950a95e7cf5&units=metric")
  .done(function( data ) {
    weather = data.main.temp < 0;
  });

$(document).ready(function(){

  $('.pizza-type label').hover(
    showHideDescription,
    showHideDescription
  );

  // Affiche le bon nombre de part
  $('.nb-parts input').on('keyup', function() {
    // Suppression de toutes les pizzas pour repartir de zero
    $(".pizza-pict").remove();
    // On enregistre dans une variable l'élément HTML qu'on va dupliquer
    var pizza = $('<span class="pizza-pict"></span>');

    slices = +$(this).val();
    // Clone permet de dupliquer l'élément HTML, sinon ce serait toujours le même
    for(i = 0; i < slices/6; i++) {
      $(this).after(pizza.clone().addClass('pizza-6'));
    }

    // la dernière pizza n'est peut être pas complète, on affiche juste le bon nombre de part
    if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);

    updateprix();
  });

  // Afficher les infos clients au clic sur le premier bouton
  $('.next-step').click(function(){
    // Remove button
    $(this).remove();
    // Affiche infos client
    $(".infos-client").show();
  });

  // Ajouter un champ adresse
  $('.add').click(function(){
    $('.infos-client .add').before('<br><input type="text"/>');
  });

  // Faire disparaitre les elements de la page et affciher le prenom
  $('.done').click(function(){
    var prenom = $('.infos-client').find('input').first().val();
    $('body').replaceWith("<p>Merci " + prenom + " ! Votre commande sera livrée dans 15 minutes</p>");

  });

  // Calculer et afficher le prix total
  function updateprix(){
    var prix = 0;

    prix += +$("input[name='type']:checked").data('price') || 0;
    prix += +$("input[name='pate']:checked").data('price') || 0;
    $("input[name='extra']:checked").each(function(i, elem) {
        prix += +$(elem).data("price");
    });

    prix *= $('.nb-parts input').val()/6;

    // 5€ en plus s'il fait froid
    if (weather) {
      prix += 5;
    }

    $('.stick-right p').text(Math.round(prix*100)/100 + " €");
  };

  // On met a jour si ça change
  $("input[data-price]").change(updateprix);

});

// $(document).ready(function(){
//
//   $('.pizza-type label').hover(
//     function(){
//     $(this).find(".description").show();
//   },
//   function(){
//   $(this).find(".description").hide();
//   }
// );
// });
